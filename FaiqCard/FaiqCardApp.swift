//
//  FaiqCardApp.swift
//  FaiqCard
//
//  Created by Faiq on 13/02/2021.
//

import SwiftUI

@main
struct FaiqCardApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
